#!/usr/bin/env python
"""
Very simple HTTP server in python.

Usage::
    ./dummy-web-server.py [<port>]

Send a GET request::
    curl http://localhost

Send a HEAD request::
    curl -I http://localhost

Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost

"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import math
import json
import base64
import requests
import subprocess
#from home/ec2-user/catcher/decode import decoderInt,decoderBool,decoderStr

# BASE^$ DECODER
translateDict = {}

def primeTranslate():
    global translateDict
    place = 0
    for x in range (0,26):
        letter = ord("A") + x
        translateDict[str(unichr(letter))] = place
        place += 1

    for x in range (0,26):
        letter = ord("a") + x
        translateDict[str(unichr(letter))] = place
        place += 1

    for x in range (0,10):
        letter = ord("0") + x
        translateDict[str(unichr(letter))] = place
        place += 1

    translateDict["+"] = place 
    place += 1
    translateDict["/"] = place

def translator(encode):
    global translateDict
    decode = []
    for x in encode:
        if x is not "=":
            decode.append(translateDict[x])
    return decode

def decoderInt(garbage,median):
    primeTranslate()
    
    # STEPS TO TRANSLATE
    # GET CHAR
    # TRANSLATE TO Decimal
    # Transform Decimal into 6 bits
    # Append all 6 bits together (48 long? = provide extra space to get to /8
    # seperate into groups of bytes (8 bits)
    # Cut off last byte (decimal)

    binstr=str(garbage)
    #print "String: " + str(binstr)
    # translate into decimal
    ba = translator(binstr)
    
    #print list(ba)
    # converts decimal into 6 bits
    result = 0
    padding = 0
    for x in ba:
        padding+=6
        result = result << 6
        result += x
    
    # convert to string of bits
    binary = str(bin(result)[2:].format(padding))

    # add appending 0s if needed (needs to be /8)
    for x in range(0,padding%8):
        binary += "0" 
        padding += 1
    while len(binary) < 40:
        padding += 1
        binary = "0" + binary

    #print "Padded BINARY: " + binary

    # cut off decimal
    if median > 0:
        result = binary[:-8*median]
        decimal = binary[-8*median:]
        #print "Split: " + result + "." + decimal
        return str(int(result,2))+"."+str(int(decimal,2))
    else:
        return str(int(binary,2))

def decoderIntSeg(garbage,seg):
    primeTranslate()
    
    # STEPS TO TRANSLATE
    # GET CHAR
    # TRANSLATE TO Decimal
    # Transform Decimal into 6 bits
    # Append all 6 bits together (48 long? = provide extra space to get to /8
    # seperate into groups of bytes (8 bits)
    # USE SEG TO FIND RIGHT PART TO PULL OUT

    binstr=str(garbage)
    #print "String: " + str(binstr)
    # translate into decimal
    ba = translator(binstr)
    
    #print list(ba)
    # converts decimal into 6 bits
    result = 0
    padding = 0
    for x in ba:
        padding+=6
        result = result << 6
        result += x
    
    # convert to string of bits
    binary = str(bin(result)[2:].format(padding))

    # add appending 0s if needed (needs to be /8)
    for x in range(0,padding%8):
        binary += "0" 
        padding += 1
    while len(binary) < 40:
        padding += 1
        binary = "0" + binary

    #print "Padded BINARY: " + binary

    # cut off segment 
    segment = binary[(seg-1)*8:seg*8]
    print "SEGMENT: " + segment
    return str(int(segment,2))

def decoderStr(garbage):
    primeTranslate()
    # NO SIZE LIMIT
    # STEPS TO TRANSLATE
    # GET CHAR
    # TRANSLATE TO Decimal
    # Transform Decimal into 6 bits
    # Append all 6 bits together (48 long? = provide extra space to get to /8
    # seperate into groups of bytes (8 bits)
    # translate to char

    binstr=str(garbage)
    #print "String: " + str(binstr)
    # translate into decimal
    ba = translator(binstr)
    
    #print list(ba)
    # converts decimal into 6 bits
    result = 0
    padding = 0
    for x in ba:
        padding+=6
        result = result << 6
        result += x

    # convert to string of bits
    binary = str(bin(result)[2:].format(padding))
    #print "THING: " + binary
    # add appending 0s if needed (needs to be /8)
    for x in range(0,padding%8):
        binary += "0" 
        padding += 1
    while len(binary) < padding:
        binary = "0" + binary
    
    start = 0
    end = 8
    word = ""
    while end < len(binary) + 1:
        piece = binary[start:end]
        word += chr(int(piece,2))    
        start+=8
        end+=8
    
    return word

def decoderBool(garbage):
    primeTranslate()
    binstr=str(garbage)
    #print "String: " + str(binstr)
    # translate into decimal
    ba = translator(binstr)
    #print str(ba)
    if ba[-1] != 0:
        return "True"
    else:
        return "False"

def translate(garbage,title):
    integerFields = ["GPS_SPEED","OBO_FULL","BATT","GPS_PDOP","MDI_EXT_BATT_VOLTAGE","MDI_DTC_NUMBER","MDI_RPM_MAX","MDI_RPM_MIN","MDI_RPM_AVERAGE","MDI_RPM_AVERAGE_RANGE_1","MDI_RPM_AVERAGE_RANGE_2","ODO_PARTIAL_METER","ODO_FULL_METER","MDI_DASHBOARD_MILEAGE","MDI_DASHBOARN_FUEL","MDI_DASHBOARD_FUEL_LEVEL","MDI_MAX_RPM_IN_LAST_OVER_RPM","MDI_OBD_SPEED","MDI_OBD_RPM","MDI_OBD_FUEL","MDI_OBD_MILEAGE","MDI_JOURNEY_TIME","MDI_IDLE_JOURNEY","MDI_DRIVING_JOURNEY","MDI_MAX_SPEED_IN_LAST_OVERSPEED","MDI_OVERSPEED_COUNTER","MDI_ODO_JOURNEY","OBD_CONNECTED_PROTOCOL","BEHAVE_ACC_X_END","BEHAVE_GPS_HEADING_END","BEHAVE_GPS_HEADING_BEGIN","BEHAVE_ELAPSED","BEHAVE_UNIQUE_ID","BEHAVE_ACC_Z_PEAK","BEHAVE_GPS_SPEED_PEAK","BEHAVE_GPS_SPEED_BEGIN","BEHAVE_GPS_HEADING_PEAK","BEHAVE_ACC_Y_BEGIN","BEHAVE_ACC_X_PEAK","BEHAVE_ACC_Z_BEGIN","BEHAVE_ACC_Z_END","BEHAVE_ACC_Z_PEAK","BEHAVE_ACC_Y_END","BEHAVE_ACC_X_END","BEHAVE_ACC_Y_PEAK","BEHAVE_GPS_SPEED_END","MDI_OBD_MILEAGE_METERS","ODO_FULL","BEHAVE_ID"]
    stringFields = ["GPRMC_VALID","ODO_PARTIAL_KM","AREA_LIST","MDI_CRASH_DETECTED","EVENT","MDI_PANIC_MESSAGE","MDI_DTC_LIST","MDI_SENSORS_RECORDER_DATA","MDI_SENSORS_RECORDER_CALIBRATION","MDI_DIAG_1","MDI_PRENDING_DTC_LIST","MDI_VEHICLE_STATE","MDI_OBD_VIN","ENH_DASHBOARD_MILEAGE","ENH_DASHBOARD_FUEL","ENH_DASHBOARD_FUEL_LEVEL","MDI_CC_DTC_LIST"]
    boolFields = ["DIO_IGNITION","MDI_PANIC_STATE","MDI_DTC_MIL","MDI_RPM_OVER","MDI_IDLE_STATE","MDI_TOW_AWAY","MDI_OVERSPEED","MDI_JOURNEY_STATE","MDI_EXT_BATT_LOW","MDI_JOURNEY_ID"]

    if title == "MDI_OBD_PID_1":
        # if OBD_PID_1, its coolant temp, decode as A,x,x,x,x
        # COOLANT
        tmp = int(decoderIntSeg(garbage,1)) -40
        #print "Test1: " + str(tmp) + "\n"
        return str(tmp)
    elif title == "MDI_OBD_PID_2":
        # INTAKE AIR TEMP
        # A - 40
        #print "Test 2: " + str(decoderStr(garbage))
        tmp = str(float(decoderIntSeg(garbage,1))-40)
        #print "Test 2a: " + tmp + "\n"
        return str(tmp)
    elif title == "MDI_OBD_PID_3":
        # Vaport Pressure
        aInt = decoderIntSeg(garbage,1) 
        bInt = decoderIntSeg(garbage,2) 
        #print "3a: " + str(aInt)
        #print "3b: " + str(bInt) + "\n"
        aInt=int(aInt)
        bInt=int(bInt)
        return str((256*aInt + bInt)/4)
    elif title == "MDI_OBD_PID_4":
        # Fuel Pressure 3xA
        #print "Test 4: " + str(decoderStr(garbage))
        #print "4: " + str(decoderIntSeg(garbage,1)) + "\n"
        return str(float(decoderIntSeg(garbage,1))*3)
    elif title == "MDI_OBD_PID_5":
        #MAF RATE
        #print "Test 5"
        aInt = decoderIntSeg(garbage,1) 
        bInt = decoderIntSeg(garbage,2) 
        #print "5a: " + str(aInt)
        #print "5b: " + str(bInt) + "\n"
        aInt=int(aInt)
        bInt=int(bInt)
        return str((256.0*aInt + bInt)/100.0)
    elif title in integerFields:
        #print "Translated: " + title
        return str(decoderInt(garbage,1))
    elif title in stringFields:
        #print "Translated: " + title
        return str(decoderStr(garbage))
    elif title in boolFields:
        #print "Translated: " + title
        return str(decoderBool(garbage))
    else:
        #subprocess.check_output("echo " + str(title) + " >> extra.txt",shell=True)
        return str(garbage)

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write("<html><body><h1>hi!</h1></body></html>")

    def do_HEAD(self):
        self._set_headers()
        
    def do_POST(self):
        self._set_headers()
        track = self.rfile.read(int(self.headers.getheader('Content-Length')))
        track = json.loads(track)
        # iterate through list of fields
        #print "<<<<<" + str(track) + "\n"
        #try:
        for item in track:
            if 'payload' in item:
                if 'fields' in item['payload']:
                    #try:
                    for x in item['payload']['fields']:
                        for y in item['payload']['fields'][x]:
                            z = item['payload']['fields'][x][y]
                            temp = translate(z,x)
                            if temp != "NULL":
                                item['payload']['fields'][x][y] = translate(z,x) 
                    #except:
                    #    pass
            # GET STREET STUFF
            if 'payload' in item:
                if 'loc' in item['payload']:
                    place = item['payload']['loc']
                    street = requests.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+str(place[1])+","+str(place[0])+"&key=AIzaSyAOmUNWSMkvMKW39Ib1cg1zI3f9HBaEPS0") 
                    stuff = street.json()
                    address = stuff['results'][0]['formatted_address']
                    street = stuff['results'][0]['address_components'][1]['long_name'] 
                    # ADD STREET STUFF
                    item['address'] = str(address)
                    item['street'] = str(street)

        # print the json to a file
        with open("/home/ec2-user/captured/Data.json","a") as outfile:
            json.dump(track,outfile)
        self.wfile.write("<html><body><h1>Process</h1></body></html>")
        
def run(server_class=HTTPServer, handler_class=S, port=9000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
