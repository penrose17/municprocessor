#!/usr/bin/python
# Georges Brantley
# this file starts the script every hour
# in a try catch in case it fails
import subprocess
from datetime import datetime

if __name__ == "__main__":
    psAux = subprocess.check_output("ps aux | grep python",shell = True)
    if "python /home/ec2-user/catcher/bigHost.py" in psAux:
        subprocess.check_output("echo \"1 Server is Up at: " +str(datetime.now()) +"\n\" >> /home/ec2-user/catcher/server.log",shell=True)
    else: 
        subprocess.check_output("echo \"0 Server is Down at: " +str(datetime.now()) +", Restarting\n\" >> /home/ec2-user/catcher/server.log",shell=True)
        thing = subprocess.check_output("/home/ec2-user/catcher/bigHost.py &",shell=True)

