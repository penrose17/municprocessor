#!/usr/bin/python2.7
translate = {}

def primeTranslate():
    global translate
    place = 0
    for x in range (0,26):
        letter = ord("A") + x
        translate[str(unichr(letter))] = place
        place += 1

    for x in range (0,26):
        letter = ord("a") + x
        translate[str(unichr(letter))] = place
        place += 1

    for x in range (0,10):
        letter = ord("0") + x
        translate[str(unichr(letter))] = place
        place += 1

    translate["+"] = place 
    place += 1
    translate["/"] = place

def translator(encode):
    global translate
    decode = []
    for x in encode:
        if x is not "=":
            decode.append(translate[x])
    return decode

def decoderInt(garbage,median):
    primeTranslate()
    
    # STEPS TO TRANSLATE
    # GET CHAR
    # TRANSLATE TO Decimal
    # Transform Decimal into 6 bits
    # Append all 6 bits together (48 long? = provide extra space to get to /8
    # seperate into groups of bytes (8 bits)
    # Cut off last byte (decimal)

    binstr=str(garbage)
    #print "String: " + str(binstr)
    # translate into decimal
    ba = translator(binstr)
    
    #print list(ba)
    # converts decimal into 6 bits
    result = 0
    padding = 0
    for x in ba:
        padding+=6
        result = result << 6
        result += x
    
    # convert to string of bits
    binary = str(bin(result)[2:].format(padding))

    # add appending 0s if needed (needs to be /8)
    for x in range(0,padding%8):
        binary += "0" 
        padding += 1
    while len(binary) < 40:
        padding += 1
        binary = "0" + binary

    #print "Padded BINARY: " + binary

    # cut off decimal
    if median > 0:
        result = binary[:-8*median]
        decimal = binary[-8*median:]
        #print "Split: " + result + "." + decimal
        return str(int(result,2))+"."+str(int(decimal,2))
    else:
        return str(int(binary,2))

def decoderStr(garbage):
    primeTranslate()
    # NO SIZE LIMIT
    # STEPS TO TRANSLATE
    # GET CHAR
    # TRANSLATE TO Decimal
    # Transform Decimal into 6 bits
    # Append all 6 bits together (48 long? = provide extra space to get to /8
    # seperate into groups of bytes (8 bits)
    # translate to char

    binstr=str(garbage)
    #print "String: " + str(binstr)
    # translate into decimal
    ba = translator(binstr)
    
    #print list(ba)
    # converts decimal into 6 bits
    result = 0
    padding = 0
    for x in ba:
        padding+=6
        result = result << 6
        result += x

    # convert to string of bits
    binary = str(bin(result)[2:].format(padding))
    #print "THING: " + binary
    # add appending 0s if needed (needs to be /8)
    for x in range(0,padding%8):
        binary += "0" 
        padding += 1
    while len(binary) < padding:
        binary = "0" + binary
    
    start = 0
    end = 8
    word = ""
    while end < len(binary) + 1:
        piece = binary[start:end]
        word += chr(int(piece,2))    
        start+=8
        end+=8
    
    return word

def decoderBool(garbage):
    primeTranslate()
    binstr=str(garbage)
    #print "String: " + str(binstr)
    # translate into decimal
    ba = translator(binstr)
    #print str(ba)
    if ba[-1] != 0:
        return "True"
    else:
        return "False"

if __name__ == "__main__":
    #print decoderInt("AAA41Q==",1)
    #print decoderBool("AQ==")
    print decoderInt("YVE9PQ==",4)
    pass
